<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                

                <form action="f_tracking" method="POST">
                <input class="form-control" id="myInput" name="resi" type="text" placeholder="lacak sesuai resi"><br/><input  type="submit" class="btn btn-sm bg-primary" value="SEARCH">
                </form><br/>
                <div id="hidden">
                <table class="table table-hover" >
                    <tbody  id="myTable">
                        <!-- looping nomer -->
                        <!-- looping nomer -->
                        <?php foreach ($track as $r) : 
                          $stat="";
                          $predict="";
                          if(strtotime("now")-strtotime($r['tanggal_pembelian'])<86400){
                            $stat="Barang Sedang Diproses Oleh Penjual";
                          }
                          else if(strtotime("now")-strtotime($r['tanggal_pembelian'])<172800){
                            $stat="Barang Sedang Dikirim";
                          }
                          else{
                            $stat="Barang Telah Sampai";
                          }
                          if($stat=="Barang Telah Sampai"){
                            $predict="-";
                          }
                          else{
                            $predict=date("Y-m-d",strtotime($r['tanggal_pembelian'])+172800);
                          }
                        ?>
                            <tr>
                              <th>Nama Barang</th>
                              <td><?= $r['nama_barang']; ?></td>
                            </tr>
                            <tr>
                              <th>Nama Penjual</th>
                               <td><?= $r['nama_penjual']; ?></td>
                            </tr>
                            <tr>
                              <th>Nama Pembeli</th>
                              <td><?= $r['nama_pembeli']; ?></td>
                            </tr>
                            <tr>
                              <th>Harga Barang</th>
                              <td><?= $r['harga']; ?></td>
                            </tr>
                            <tr>
                              <th>Jumlah Barang</th>
                              <td><?= $r['jumlah']; ?></td>
                            </tr>
                            <tr>
                              <th>Total</th>
                              <td><?= $r['jumlah']; ?></td>
                            </tr>
                            <tr>
                              <th>Status Pengiriman</th>
                              <td><?= $stat; ?></td>
                            </tr>
                            <tr>
                              <th>Prediksi Sampai Pada Tujuan</th>
                              <td><?= $predict; ?></td>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>
              </div>


            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->




  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<script>
    function demoFromHTML() {
        var pdf = new jsPDF('p', 'pt', 'letter');
        // source can be HTML-formatted string, or a reference
        // to an actual DOM element from which the text will be scraped.
        source = $('#hidden')[0];

        // we support special element handlers. Register them with jQuery-style 
        // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
        // There is no support for any other type of selectors 
        // (class, of compound) at this time.
        specialElementHandlers = {
            // element with id of "bypass" - jQuery style selector
            '#bypassme': function (element, renderer) {
                // true = "handled elsewhere, bypass text extraction"
                return true
            }
        };
        margins = {
            top: 80,
            bottom: 60,
            left: 40,
            width: 522
        };
        // all coords and widths are in jsPDF instance's declared units
        // 'inches' in this case
        pdf.fromHTML(
            source, // HTML string or DOM elem ref.
            margins.left, // x coord
            margins.top, { // y coord
                'width': margins.width, // max width of content on PDF
                'elementHandlers': specialElementHandlers
            },

            function (dispose) {
                // dispose: object with X, Y of the last line add to the PDF 
                //          this allow the insertion of new lines after html
                pdf.save('Laporan ekspedisi.pdf');
            }, margins
        );
    }
</script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>