<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <!-- menampilkan pesan eror jika form_validasi tidak lolos -->
                <?= $this->session->flashdata('message'); ?>

                <!-- menampilkan pesan sukses setelah role berhasil di insert ke db -->
                <!-- tombol untuk memunculkan modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                  + Data Transaksi
                </button>
                <!-- tombol untuk memunculkan modal -->
                <input class="form-control" id="myInput" type="text" placeholder="Search..">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Pembeli</th>
                            <th scope="col">Jenis Cucian</th>
                            <th scope="col">Jenis Kendaraan</th>
                            <th scope="col">Jenis Pembayaran</th>
                            <th scope="col">Harga</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody  id="myTable">
                        <!-- looping nomer -->
                        <?php $i = 1;?>
                        <!-- looping nomer -->
                        <?php foreach ($users as $r) : 
                        ?>
                            <tr>
                                <th scope="row"><?= $i; ?></th> <!-- menampilkan hasil looping nmr -->
                                <td><?= $r['book_nama']; ?></td>
                                <td><?= $r['jenis_cucian']; ?></td>
                                <td><?= $r['jenis_kendaraan']; ?></td>
                                <td><?= $r['jenis_pembayaran']; ?></td>
                                <td><?= $r['harga']; ?></td>
                                <td><?= $r['tanggal']; ?></td>
                                <td>
                                
                                    <button type="button" class="btn btn-sm bg-danger" data-toggle="modal" data-target="#exampleModals<?=$i;?>">DELETE</button>
                                    <button type="button" class="btn btn-sm bg-warning" data-toggle="modal" data-target="#exampleModalss<?=$i;?>">EDIT</button>
                                </td>
                            </tr>
                            <!-- looping nomer -->
                            <!-- looping nomer -->
                           <div class="modal fade" id="exampleModals<?=$i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <form method="post" action="<?= base_url("admin/delete_transaksi"); ?>">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Hapus Data Kategori</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                         Apakah anda yakin ingin menghapus data transaksi ini?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <input type="text" value="<?= $r['book_id']; ?>" name="del"style="display:none">
                                  <input  type="submit" class="btn btn-sm bg-danger" value="DELETE">
                                </div>
                              </div>
                               </form>
                            </div>
                          </div>
                          
                          
                          
                          <div class="modal fade" id="exampleModalss<?=$i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <form method="post" action="<?= base_url("admin/edit_transaksi"); ?>">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">EDIT Data Kategori</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                         <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Nama Pembeli:</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="book_nama" id="" value="<?= $r['book_nama']?>" class="form-control"  placeholder="Masukan Nama Penjual">
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Jenis Cucian:</label>
                                            <div class="col-sm-6">
                                                <select name="jenis_cucian" id="" value="<?= $r['jenis_cucian']?>" class="form-control">
                                                  <?php foreach($jenis_cucian as $p):?>
                                                  <option value="<?=$p['jenis_nama']?>"><?=$p['jenis_nama']?></option>
                                                  <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Jenis Kendaraan:</label>
                                            <div class="col-sm-6">
                                                <select name="jenis_kendaraan" value="<?= $r['jenis_kendaraan']?>" id="" class="form-control">
                                                  <?php foreach($jenis_kendaraan as $p):?>
                                                  <option value="<?=$p['kendaraan_nama']?>"><?=$p['kendaraan_nama']?></option>
                                                  <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Jenis Pembayaran:</label>
                                            <div class="col-sm-6">
                                                <select name="jenis_pembayaran" id="" value="<?= $r['jenis_pembayaran']?>" class="form-control">
                                                  <?php foreach($jenis_pembayaran as $p):?>
                                                  <option value="<?=$p['metode_nama']?>"><?=$p['metode_nama']?></option>
                                                  <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label"  style="margin-left: 150px;">Harga:</label>
                                            <div class="col-sm-6">
                                                <input type="number" name="harga"  value="<?= $r['harga']?>" id="" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <input type="text" value="<?= $r['book_id']; ?>" name="del"style="display:none">
                                  <input  type="submit" class="btn btn-sm bg-warning" value="EDIT">
                                </div>
                              </div>
                               </form>
                            </div>
                          </div>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>



            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <form method="post" action="<?= base_url("admin/add_transaksi"); ?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Transaksi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                                        
                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Nama Pembeli:</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="book_nama" id="" class="form-control"  placeholder="Masukan Nama Penjual">
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Jenis Cucian:</label>
                                            <div class="col-sm-6">
                                                <select name="jenis_cucian" id="" class="form-control">
                                                  <?php foreach($jenis_cucian as $p):?>
                                                  <option value="<?=$p['jenis_nama']?>"><?=$p['jenis_nama']?></option>
                                                  <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Jenis Kendaraan:</label>
                                            <div class="col-sm-6">
                                                <select name="jenis_kendaraan" id="" class="form-control">
                                                  <?php foreach($jenis_kendaraan as $p):?>
                                                  <option value="<?=$p['kendaraan_nama']?>"><?=$p['kendaraan_nama']?></option>
                                                  <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Jenis Pembayaran:</label>
                                            <div class="col-sm-6">
                                                <select name="jenis_pembayaran" id="" class="form-control">
                                                  <?php foreach($jenis_pembayaran as $p):?>
                                                  <option value="<?=$p['metode_nama']?>"><?=$p['metode_nama']?></option>
                                                  <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Harga:</label>
                                            <div class="col-sm-6">
                                                <input type="number" name="harga" id="" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input  type="submit" class="btn btn-primary" value="Simpan data">
      </div>
    </div>
     </form>
  </div>
</div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>