<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <!-- menampilkan pesan eror jika form_validasi tidak lolos -->
                <?= $this->session->flashdata('message'); ?>

                <!-- menampilkan pesan sukses setelah role berhasil di insert ke db -->
                <!-- tombol untuk memunculkan modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                  + Data Produk
                </button>
                <!-- tombol untuk memunculkan modal -->
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Produk</th>
                            <th scope="col">Kategori</th>
                            <th scope="col">Harga</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- looping nomer -->
                        <?php $i = 1;?>
                        <!-- looping nomer -->
                        <?php foreach ($produk as $r) : ?>
                            <tr>
                                <th scope="row"><?= $i; ?></th> <!-- menampilkan hasil looping nmr -->
                                <td><?= $r['nama']; ?></td>
                                <td><?= $r['kategori']; ?></td>
                                <td><?= $r['harga']; ?></td>
                                <td>
                                    <button type="button" class="btn btn-sm bg-danger" data-toggle="modal" data-target="#exampleModals<?=$i;?>">DELETE</button>
                                    <button type="button" class="btn btn-sm bg-warning" data-toggle="modal" data-target="#exampleModalss<?=$i;?>">EDIT</button>
                                </td>
                            </tr>
                           <div class="modal fade" id="exampleModals<?=$i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <form method="post" action="<?= base_url("admin/delete_produk"); ?>">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Hapus Data Produk</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                         Apakah anda yakin ingin menghapus data <?= $r['nama']; ?>?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <input type="text" value="<?= $r['id']; ?>" name="del"style="display:none">
                                  <input  type="submit" class="btn btn-sm bg-danger" value="DELETE">
                                </div>
                              </div>
                               </form>
                            </div>
                          </div>
                          
                          <div class="modal fade" id="exampleModalss<?=$i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <form method="post" action="<?= base_url("admin/edit_produk"); ?>">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">EDIT Data Produk</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                         <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Nama Produk</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="nama" id="" class="form-control" value="<?= $r['nama']; ?>" placeholder="Masukan Nama Produk">
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label"   style="margin-left: 150px;">Kategori</label>
                                            <div class="col-sm-6">
                                                <select type="text" name="kategori" id=""  value="<?= $r['kategori']; ?>" class="form-control" >
                                                  <?php foreach ($kategori as $k):?>
                                                    <option value="<?=$k['nama']?>"><?=$k['nama']?></option>
                                                  <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label"  style="margin-left: 150px;">Harga <small>(Rp)</small></label>
                                            <div class="col-sm-6">
                                                <input type="number" name="harga" id="" class="form-control" value="<?= $r['harga']; ?>">
                                            </div>
                                        </div>
                                        
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <input type="text" value="<?= $r['id']; ?>" name="del"style="display:none">
                                  <input  type="submit" class="btn btn-sm bg-warning" value="EDIT">
                                </div>
                              </div>
                               </form>
                            </div>
                          </div>
                            <!-- looping nomer -->
                            <?php $i++; ?>
                            <!-- looping nomer -->
                        <?php endforeach; ?>
                    </tbody>
                </table>



            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <form method="post" action="<?= base_url("admin/add_data_produk"); ?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                                         <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Nama Produk</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="nama" id="" class="form-control"  placeholder="Masukan Nama Produk">
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label"   style="margin-left: 150px;">Kategori</label>
                                            <div class="col-sm-6">
                                                <select type="text" name="kategori" id=""   class="form-control" >
                                                  <?php foreach ($kategori as $k):?>
                                                    <option value="<?=$k['nama']?>"><?=$k['nama']?></option>
                                                  <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label"  style="margin-left: 150px;">Harga <small>(Rp)</small></label>
                                            <div class="col-sm-6">
                                                <input type="number" name="harga" id="" class="form-control" >
                                            </div>
                                        </div>
                                        
                                </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input  type="submit" class="btn btn-primary" value="Simpan data">
      </div>
    </div>
     </form>
  </div>
</div>
