<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <!-- menampilkan pesan eror jika form_validasi tidak lolos -->
                <?= $this->session->flashdata('message'); ?>

                <!-- menampilkan pesan sukses setelah role berhasil di insert ke db -->
                <!-- tombol untuk memunculkan modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                  + Data Pelanggan
                </button>
                <!-- tombol untuk memunculkan modal -->
                <input class="form-control" id="myInput" type="text" placeholder="Search..">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">Tanggal Lahir</th>
                            <th scope="col">Email</th>
                            <th scope="col">Nomor Member</th>
                            <th scope="col">Gender</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody  id="myTable">
                        <!-- looping nomer -->
                        <?php $i = 1;?>
                        <!-- looping nomer -->
                        <?php foreach ($users as $r) : ?>
                            <tr>
                                <th scope="row"><?= $i; ?></th> <!-- menampilkan hasil looping nmr -->
                                <td><?= $r['nama_pelanggan']; ?></td>
                                <td><?= $r['alamat_pelanggan']; ?></td>
                                <td><?= $r['tanggal_lahir']; ?></td>
                                <td><?= $r['email']; ?></td>
                                <td><?= $r['no_member']; ?></td>
                                <td><?= $r['gender']; ?></td>
                                <td><?= $r['phone']; ?></td>
                                <td>
                                
                                    <button type="button" class="btn btn-sm bg-danger" data-toggle="modal" data-target="#exampleModals<?=$i;?>">DELETE</button>
                                    <button type="button" class="btn btn-sm bg-warning" data-toggle="modal" data-target="#exampleModalss<?=$i;?>">EDIT</button>
                                </td>
                            </tr>
                            <!-- looping nomer -->
                            <!-- looping nomer -->
                           <div class="modal fade" id="exampleModals<?=$i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <form method="post" action="<?= base_url("admin/delete_pelanggan"); ?>">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Hapus Data Pelanggan</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                         Apakah anda yakin ingin menghapus data <?= $r['nama_pelanggan']; ?>?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <input type="text" value="<?= $r['pelanggan_id']; ?>" name="del"style="display:none">
                                  <input  type="submit" class="btn btn-sm bg-danger" value="DELETE">
                                </div>
                              </div>
                               </form>
                            </div>
                          </div>
                          
                          
                          
                          <div class="modal fade" id="exampleModalss<?=$i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <form method="post" action="<?= base_url("admin/edit_pelanggan"); ?>">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">EDIT Data Pelanggan</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                         <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Nama</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="nama" id="" class="form-control" value="<?= $r['nama_pelanggan']; ?>" placeholder="Masukan Nama Pelanggan">
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label"   style="margin-left: 150px;">Alamat</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="alamat" id=""  value="<?= $r['alamat_pelanggan']; ?>" class="form-control" placeholder="Masukan Alamat Pelanggan">
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label"  style="margin-left: 150px;">Tanggal Lahir</label>
                                            <div class="col-sm-6">
                                                <input type="date" name="tanggal" id="" class="form-control" value="<?= $r['tanggal_lahir']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Email</label>
                                            <div class="col-sm-6">
                                                <input type="email" name="email" id="" class="form-control"  placeholder="Masukan Email Pelanggan" value="<?= $r['email']; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left:-120px;" value="<?= $r['no_member']; ?>">
                                            <label class="col-sm-2 col-form-label"   style="margin-left: 150px;">Nomor Member</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="no_member" id=""  class="form-control" placeholder="Masukan Nomor Member Pelanggan" value="<?= $r['no_member']; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label"  style="margin-left: 150px;">Gender</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="gender" id="" class="form-control" placeholder="Masukan Gender Pelanggan" value="<?= $r['gender']; ?>">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label"  style="margin-left: 150px;">Phone</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="phone" id="" class="form-control" placeholder="Masukan Nomor Telpon Pelanggan" value="<?= $r['phone']; ?>">
                                            </div>
                                        </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <input type="text" value="<?= $r['pelanggan_id']; ?>" name="del"style="display:none">
                                  <input  type="submit" class="btn btn-sm bg-warning" value="EDIT">
                                </div>
                              </div>
                               </form>
                            </div>
                          </div>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>



            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <form method="post" action="<?= base_url("admin/add_data_pelanggan"); ?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pelanggan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Nama</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="nama" id="" class="form-control"  placeholder="Masukan Nama Pelanggan">
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label"   style="margin-left: 150px;">Alamat</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="alamat" id=""  class="form-control" placeholder="Masukan Alamat Pelanggan">
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label"  style="margin-left: 150px;">Tanggal Lahir</label>
                                            <div class="col-sm-6">
                                                <input type="date" name="tanggal" id="" class="form-control">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label" style="margin-left: 150px;">Email</label>
                                            <div class="col-sm-6">
                                                <input type="email" name="email" id="" class="form-control"  placeholder="Masukan Email Pelanggan">
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label"   style="margin-left: 150px;">Nomor Member</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="no_member" id=""  class="form-control" placeholder="Masukan Nomor Member Pelanggan">
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label"  style="margin-left: 150px;">Gender</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="gender" id="" class="form-control" placeholder="Masukan Gender Pelanggan">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row" style="margin-left:-120px;">
                                            <label class="col-sm-2 col-form-label"  style="margin-left: 150px;">Phone</label>
                                            <div class="col-sm-6">
                                                <input type="text" name="phone" id="" class="form-control" placeholder="Masukan Nomor Telpon Pelanggan">
                                            </div>
                                        </div>
                                </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input  type="submit" class="btn btn-primary" value="Simpan data">
      </div>
    </div>
     </form>
  </div>
</div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>