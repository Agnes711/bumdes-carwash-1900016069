<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
        <img src="<?= base_url('assets'); ?>/dist/img/AdminLTELogo.png" alt="Sisfo Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Mr. Kurir</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
                <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                    <li class="nav-header">Barang</li>
                        <!-- menampilkan sidebar setelah dilakukan looping -->
                        <li class="nav-item">
                            <a href="<?= base_url("user/data_produk"); ?>" class="nav-link">
                                <p>
                                    Data Produk
                                    <span class="badge badge-info right"></span>
                                </p>
                            </a>
                        </li>
                        
                        
                        
                        <li class="nav-item">
                            <a href="<?= base_url("user/data_pelanggan"); ?>" class="nav-link">
                                <p>
                                    Data Pelanggan
                                    <span class="badge badge-info right"></span>
                                </p>
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a href="<?= base_url("user/kategori"); ?>" class="nav-link">
                                <p>
                                    Kategori
                                    <span class="badge badge-info right"></span>
                                </p>
                            </a>
                        </li>
                        
                        
                        <li class="nav-item">
                            <a href="<?= base_url("user/transaksi"); ?>" class="nav-link">
                                <p>
                                    transaksi
                                    <span class="badge badge-info right"></span>
                                </p>
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a href="<?= base_url("user/tracking"); ?>" class="nav-link">
                                <p>
                                    tracking
                                    <span class="badge badge-info right"></span>
                                </p>
                            </a>
                        </li>
                        
                        
                <li class="nav-item">
                    <a href="<?= base_url('auth/logout'); ?>" class="nav-link">
                        <i class="nav-icon fa fa-sign-out-alt"></i>
                        <p>
                            Logout
                        </p>
                    </a>
                </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>