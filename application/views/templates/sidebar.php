<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
        <img src="<?= base_url('assets'); ?>/dist/img/AdminLTELogo.png" alt="Sisfo Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">bumdes carwash</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
                <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                    <li class="nav-header">Barang</li>
                        <!-- menampilkan sidebar setelah dilakukan looping -->
                        
                        
                        <li class="nav-item">
                            <a href="<?= base_url("admin/kendaraan"); ?>" class="nav-link">
                                <p>
                                    Jenis Kendaraan
                                    <span class="badge badge-info right"></span>
                                </p>
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a href="<?= base_url("admin/jenis"); ?>" class="nav-link">
                                <p>
                                    Jenis Cucian
                                    <span class="badge badge-info right"></span>
                                </p>
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a href="<?= base_url("admin/metode"); ?>" class="nav-link">
                                <p>
                                    Metode Pembayaran
                                    <span class="badge badge-info right"></span>
                                </p>
                            </a>
                        </li>
                        
                        
                        <li class="nav-item">
                            <a href="<?= base_url("admin/transaksi"); ?>" class="nav-link">
                                <p>
                                    Transaksi
                                    <span class="badge badge-info right"></span>
                                </p>
                            </a>
                        </li>
                        
                        
                        
                        
                <li class="nav-item">
                    <a href="<?= base_url('auth/logout'); ?>" class="nav-link">
                        <i class="nav-icon fa fa-sign-out-alt"></i>
                        <p>
                            Logout
                        </p>
                    </a>
                </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>