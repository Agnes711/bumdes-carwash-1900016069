<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        if ($this->session->userdata('username')) { //jika di sesion ada email akan di redirect ke kontroler user / index agar tidak bisa mengakses auth
            redirect('user');
        }
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email'); //trim = menghilangkan spasi jika ada kesalahan penulisan, required = wajib diisi, valid_email = format email yang benar
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == false) {  //jika form_validation tidak berjalan maka       
            $data['title'] = 'Login Page';
            $this->load->view('templates/auth_header', $data); //template header : struktur folder -> file
            $this->load->view('auth/login'); //isi utama
            $this->load->view('templates/auth_footer'); //template footer
        } else { //jika form_validasi berhasil maka lanjut
            $this->_login(); // mengakses method _login (hanya bisa diakses di kontroler ini, tidak bisa diakses kontroler lain)
        }
    }

    private function _login()
    {
        $email = $this->input->post('email'); //mengambil inputan email dan disimpan ke parameter $email
        $password = $this->input->post('password'); //mengambil inputan password dan disimpan ke parameter $password

        $user = $this->db->get_where('user', ['username' => $email])->row_array(); //mengecek di db tabel user kolom email , row_array = untuk menhambil satu baris saja

        if ($user) { //jika usernya ada
  
                if (password_verify($password, $user['password'])) { //cek password , jika di db ada usernya tapi password salah
                    $data = [ //persiapan menyimpan data ke session agar bisa digunakan di halaman lain
                        'email' => $user['username'], //mengambil data dari $user 
                        'role' => $user['role'] //mengambil data daro $user
                    ];
                    $this->session->set_userdata($data); //menyimpan data ke $session
                    if ($user['role'] == "Owner") { 
                        redirect('admin'); //diarahkan ke controler admin
                    } else {
                        redirect('user'); //diarahkan ke controler user
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Password Salah !
               </div>'); //membuat pesan jika ada data di db tetapi passwordnya salah, yang akan ditampilkan di halaman login
                    redirect('auth'); //meredirect ke halam login struktur : 
                }
            }
        else {
            //jika usernya tidak ada
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Email Tidak Terdaftar ! T_T
           </div>'); //membuat pesan jika tidak ada user di db yang akan ditampilkan di halaman login
            redirect('auth'); //meredirect ke halam login struktur : 
        }
    }

    public function registration()
    {
        

        $this->form_validation->set_rules('name', 'Name', 'required|trim'); //trim = menghilangkan spasi jika ada kesalahan penulisan, required = wajib diisi
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[8]|matches[password2]'); //mengecek agar password = retype password
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]'); //mengecek agar retype password = password
        if ($this->form_validation->run() == false) { //jika form_validasi tidak berjalan 
            $data['title'] = 'User Registration';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/registration');
            $this->load->view('templates/auth_footer');
        } else { //jika form_validation berjalan maka
            $email = $this->input->post('email', true);
            $get = [ //menyiapkan data untuk di insert ke database !
                'username' => htmlspecialchars($this->input->post('email', true)),
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'role' => $this->input->post('role'),
                'nama_karyawan' => htmlspecialchars($this->input->post('name', true)),
                'alamat_karyawan' => htmlspecialchars($this->input->post('address', true))
            ];
            $this->db->insert('user', $get); //menyimpan data ke dalam database di tabel user_token (lebih bagus gunakan model)
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Congratulation ! Your Account Has Been Created . . . (☞ﾟヮﾟ)☞ 
            </div>'); //membuat pesan setelah data berhasil disimpan ke database yang akan ditampilkan di halaman login
            redirect('auth'); //meredirect ke halam login setelah berhasil menyimpan data ke database
        }
    }

    private function _sendEmail($token, $type)
    {
        $config = [
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.elasticemail.com',
            'smtp_port' => 2525,
            'smtp_user' => 'sistemkepegawaian1@gmail.com',
            'smtp_pass' => '7D804D5A121102796D4AA6BE312B5891E77A',
            'crlf' => "\r\n",
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
        ];

        $this->email->initialize($config);
        $this->email->set_newline("\r\n");

        $this->load->library('email', $config);

        $this->email->from('sistemkepegawaian1@gmail.com', 'Sisfo Kepegawaian');
        $this->email->to($this->input->post('email'));

        if ($type == 'verify') {
            $this->email->subject('Account Verification');
            $this->email->message('
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <style type="text/css">
        body,
        html {
            margin: 0px;
            padding: 0px;
            -webkit-font-smoothing: antialiased;
            text-size-adjust: none;
            width: 100% !important;
        }
        
        table td,
        table {}
        
        #outlook a {
            padding: 0px;
        }
        
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }
        
        .ExternalClass {
            width: 100%;
        }
        
        @media only screen and (max-width: 480px) {
            table,
            table tr td,
            table td {
                width: 100%;
            }
            table tr td table.edsocialfollowcontainer {
                width: auto;
            }
            img {
                width: inherit;
            }
            .layer_2 {
                max-width: 100% !important;
            }
            .edsocialfollowcontainer table {
                max-width: 25% !important;
            }
            .edsocialfollowcontainer table td {
                padding: 10px !important;
            }
            .edsocialfollowcontainer table {
                max-width: 25% !important;
            }
            .edsocialfollowcontainer table td {
                padding: 10px !important;
            }
        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i &subset=cyrillic,latin-ext" data-name="open_sans" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
</head>

<body style="padding:0; margin: 0;background: #e4e6ec">
    <table style="height: 100%; width: 100%; background-color: #e4e6ec;" align="center">
        <tbody>
            <tr>
                <td valign="top" id="dbody" data-version="2.31" style="width: 100%; height: 100%; background-color: #000000; padding-top: 50px; padding-bottom: 50px;">
                    <!--[if (gte mso 9)|(IE)]><table align="center" style="max-width:492px" width="492" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                    <!--[if (gte mso 9)|(IE)]><table align="center" style="max-width:600px" width="600" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                    <table class="layer_1" align="center" border="0" cellpadding="0" cellspacing="0" style="border-width: 0px; border-color: #000000; border-style: none; max-width: 492px; box-sizing: border-box; width: 100%; margin: 0px auto;">
                        <tbody>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 10px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="display: inline-block; vertical-align: top; width: 100%; max-width: 600px;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="edimg" style="text-align: center; padding: 20px; font-size: 12px; box-sizing: border-box;">
                                                        <img src="https://api.elasticemail.com/userfile/1239785c-6f54-418e-bbb5-c353f7812765/a1c42c509a4c4ce089c995c71ef3140a.png" alt="Image" width="210" style="border-width: 0px; border-style: none; max-width: 210px; width: 100%;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 600px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 600px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="edtext" style="padding: 0px; color: #ffffff; font-size: 14px; font-family: Lato, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: left; word-break: break-word; direction: ltr; box-sizing: border-box;">
                                                        <h2>Almost done,
                                                        </h2>
                                                        <p style="margin: 0px; padding: 0px;">To complete your account registration, we just need to verify your email address
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 10px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 246px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse; width: 100%; background-color: #000000;">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="breakline" style="padding: 0px;">
                                                        <p style="border-style: solid none none; border-width: 2px 0px 0px; margin: 36px 0px; border-top-color: #808080; padding: 0px;">&nbsp;
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 246px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" class="edcontent" style="border-collapse: collapse; width: 100%;">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="edbutton" style="padding: 20px;">
                                                        <table cellspacing="0" cellpadding="0" style="text-align: center;" align="right">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="center" style="border-radius: 22px; padding: 10px; background: #ffffff;">
                                                                        <a href="' . base_url() . 'auth/verify?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '" style="color: #000000; font-size: 14px; font-family: &quot;Arial Black&quot;, Gadget, sans-serif; font-weight: normal; text-decoration: none; display: inline-block;"><span style="color: #000000;">&nbsp;Verify Now</span></a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- [if (gte mso 9)|(IE)]></td></tr></table><![endif] -->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 600px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="edtext" style="padding: 0px; color: #ffffff; font-size: 14px; font-family: Lato, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: left; word-break: break-word; direction: ltr; box-sizing: border-box;">
                                                        <p style="margin: 0px; padding: 0px;">Once verified, you can start using all of features.
                                                        </p>
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <p style="margin: 0px; padding: 0px;">Button not working?
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="edtext" style="padding: 0px; color: #ffffff; font-size: 14px; font-family: Lato, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: left; word-break: break-word; direction: ltr; box-sizing: border-box;">
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <table>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <table>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table>
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <a href="' . base_url() . 'auth/verify?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '" target="_blank" style="color: #5457ff; text-decoration: none;">Klik here</a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 16px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="breakline" style="padding: 0px;">
                                                        <p style="border-style: solid none none; border-width: 2px 0px 0px; margin: 8px 0px; border-top-color: #00ff13; padding: 0px;">&nbsp;
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" class="edcontent" style="border-collapse: collapse; width: 100%;">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="edtext" style="padding: 48px; color: #ffffff; font-size: 14px; font-family: Lato, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: left; word-break: break-word; direction: ltr; box-sizing: border-box;">
                                                        <p class="text-center" style="text-align: center; margin: 0px; padding: 0px;">
                                                            <span style="color: #000000;">&nbsp;</span>
                                                            <font color="#00ff13">
                                                                <b>SISTEM INFORMASI KEPEGAWAIAN
                                                                </b>
                                                            </font>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="breakline" style="padding: 0px;">
                                                        <p style="border-style: solid none none; border-width: 2px 0px 0px; margin: 8px 0px; border-top-color: #00ff13; padding: 0px;">&nbsp;
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 16px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 16px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="edimg" style="padding: 0px; box-sizing: border-box; text-align: center;">
                                                        <img src="https://api.elasticemail.com/userfile/a18de9fc-4724-42f2-b203-4992ceddc1de/gifek.gif" alt="Image" width="600" style="border-width: 0px; border-style: none; max-width: 600px; width: 100%;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #c400a8; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow text-center" valign="top" align="center" style="background-color: #000000; text-align: center; box-sizing: border-box; font-size: 0px;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2 text-center" style="max-width: 492px; text-align: center; display: inline-block; vertical-align: top; width: 100%;">
                                        <table class="edcontent" style="border-collapse: collapse;width:100%" border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td class="edtext text-center" valign="top" style="padding: 0px; text-align: center; color: #5f5f5f; font-size: 12px; font-family: &quot;Open Sans&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; word-break: break-word; direction: ltr; box-sizing: border-box;">
                                                        <p style="font-size: 11px; margin: 0px; padding: 0px;">
                                                            <span style="color: #ffffff;">
                                                                <span style="color: #a1a1a1;">If you no longer wish to receive mail from us, you can</span>&nbsp;
                                                            <a href="{unsubscribe}" style="color: #5457ff; text-decoration: none;"><span style="font-size: 11px; color: #00ff13;">unsubscribe</span></a>
                                                            <br>
                                                            <br></span>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #c400a8; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 20px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 61.5px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 61.5px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="layer_2" style="max-width: 61.5px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="edimg" style="padding: 0px; box-sizing: border-box; text-align: center;">
                                                        <img src="https://api.elasticemail.com/userfile/a18de9fc-4724-42f2-b203-4992ceddc1de/fb-template.png" alt="Image" width="40" style="border-width: 0px; border-style: none; max-width: 40px; width: 100%;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="layer_2" style="max-width: 61.5px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="edimg" style="padding: 0px; box-sizing: border-box; text-align: center;">
                                                        <img src="https://api.elasticemail.com/userfile/a18de9fc-4724-42f2-b203-4992ceddc1de/jutuby.png" alt="Image" width="40" style="border-width: 0px; border-style: none; max-width: 40px; width: 100%;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="layer_2" style="max-width: 61.5px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="edimg" style="padding: 0px; box-sizing: border-box; text-align: center;">
                                                        <img src="https://api.elasticemail.com/userfile/a18de9fc-4724-42f2-b203-4992ceddc1de/twit.png" alt="Image" width="47" style="border-width: 0px; border-style: none; max-width: 47px; width: 100%;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="layer_2" style="max-width: 61.5px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="edimg" style="padding: 0px; box-sizing: border-box; text-align: center;">
                                                        <img src="https://api.elasticemail.com/userfile/a18de9fc-4724-42f2-b203-4992ceddc1de/Mask_Group_1516.png" alt="Image" width="36" style="border-width: 0px; border-style: none; max-width: 36px; width: 100%;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="layer_2" style="max-width: 61.5px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="layer_2" style="max-width: 61.5px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 0px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="drow" valign="top" align="center" style="background-color: #000000; box-sizing: border-box; font-size: 0px; text-align: center;">
                                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td valign="top"><![endif]-->
                                    <div class="layer_2" style="max-width: 492px; display: inline-block; vertical-align: top; width: 100%;">
                                        <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" class="emptycell" style="padding: 20px;">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
            ');
        } else if ($type == 'forgot') {
            $this->email->subject('Reset Password');
            $this->email->message('Click this link to reset your password : <a href="' . base_url() . 'auth/resetpassword?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Reset Password</a>');
        }

        if ($this->email->send()) {
            return true;
        } else {
            echo $this->email->print_debugger();
        }
    }


    public function verify()
    {
        $email = $this->input->get('email');
        $token = $this->input->get('token');

        $user = $this->db->get_where('user', ['email' => $email])->row_array();

        if ($user) {
            $user_token = $this->db->get_where('user_token', ['token' => $token])->row_array();

            if ($user_token) {
                if (time() - $user_token['date_created'] < (60 * 60 * 24 * 3)) {
                    $this->db->set('is_active', 1);
                    $this->db->where('email', $email);
                    $this->db->update('user');

                    $this->db->delete('user_token', ['email' => $email]);

                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    ' . $email . ' has been activated! Please Login.
                    </div>'); //membuat pesan jika email sudah diaktivasi
                    redirect('auth'); //meredirect ke halam login setelah berhasil menghilangkan data session
                } else {

                    $this->db->delete('user', ['email' => $email]);
                    $this->db->delete('user_token', ['email' => $email]);
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    Activation fail ! Token expired. !!
                    </div>'); //membuat pesan jika email salah / ngawur
                    redirect('auth'); //meredirect ke halam login setelah berhasil menghilangkan data session
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Activation fail ! Wrong token !!
                </div>'); //membuat pesan jika email salah / ngawur
                redirect('auth'); //meredirect ke halam login setelah berhasil menghilangkan data session
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Activation fail ! Wrong email !!
            </div>'); //membuat pesan jika email salah / ngawur
            redirect('auth'); //meredirect ke halam login setelah berhasil menghilangkan data session
        }
    }

    public function forgotPassword()
    {
        if ($this->session->userdata('email')) { //jika di sesion ada email akan di redirect ke kontroler user / index agar tidak bisa mengakses auth
            redirect('auth/blocked');
        }
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Forgot Password';
            $this->load->view('templates/auth_header', $data); //template header : struktur folder -> file
            $this->load->view('auth/forgot-password'); //isi utama
            $this->load->view('templates/auth_footer'); //template footer
        } else {
            $email = $this->input->post('email');
            $user = $this->db->get_where('user', ['email' => $email, 'is_active' => 1])->row_array();

            if ($user) {
                $token = base64_encode(random_bytes(32));
                $user_token = [
                    'email' => $email,
                    'token' => $token,
                    'date_created' => time()
                ];

                $this->db->insert('user_token', $user_token);
                $this->_sendEmail($token, 'forgot');

                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Check your email to reset password !
                </div>'); //membuat pesan jika email salah / ngawur
                redirect('auth/forgotpassword'); //meredirect ke halam login setelah berhasil menghilangkan data session
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Email is not regiser or not activated .
                </div>'); //membuat pesan jika email salah / ngawur
                redirect('auth/forgotpassword'); //meredirect ke halam login setelah berhasil menghilangkan data session
            }
        }
    }

    public function resetPassword()
    {
        if ($this->session->userdata('email')) { //jika di sesion ada email akan di redirect ke kontroler user / index agar tidak bisa mengakses auth
            redirect('auth/blocked');
        }
        $email = $this->input->get('email'); //get untuk mengambil dari url
        $token = $this->input->get('token'); //get untuk mengambil dari url

        $user = $this->db->get_where('user', ['email' => $email])->row_array(); //cek di db tabel user kolom email ada tidak email yang sama seperti di url
        if ($user) {
            $user_token = $this->db->get_where('user_token', ['token' => $token])->row_array(); //cek di db tabel user_token kolom token ada tidak token yang sama seperti di url
            if ($user_token) {
                $this->session->set_userdata('reset_email', $email);
                $this->changePassword();
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Reset password failed ! Wrong token !!
                </div>'); //membuat pesan jika email salah / ngawur
                redirect('auth/forgotpassword'); //meredirect ke halam login setelah berhasil menghilangkan data session
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Reset password failed ! Email is not regiser !!
            </div>'); //membuat pesan jika email salah / ngawur
            redirect('auth/forgotpassword'); //meredirect ke halam login setelah berhasil menghilangkan data session
        }
    }


    public function changePassword()
    {
        if ($this->session->userdata('email')) { //jika di sesion ada email akan di redirect ke kontroler user / index agar tidak bisa mengakses auth
            redirect('auth/blocked');
        }
        if (!$this->session->userdata('reset_email')) {
            redirect('auth');
        }
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|matches[password1]'); //rule validasi password agar sama dengan repeat password
        $this->form_validation->set_rules('password1', 'Repeat Password', 'trim|required|min_length[8]|matches[password]'); //rule validasi repeat password agar sama dengan password
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Change Password';
            $this->load->view('templates/auth_header', $data); //template header : struktur folder -> file
            $this->load->view('auth/change-password'); //isi utama
            $this->load->view('templates/auth_footer'); //template footer
        } else {
            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $email = $this->session->userdata('reset_email');

            $this->db->set('password', $password);
            $this->db->where('email', $email);
            $this->db->update('user');

            $this->session->unset_userdata('reset_email');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Password has been change!
            </div>'); //membuat pesan jika email salah / ngawur
            redirect('auth/forgotpassword'); //meredirect ke halam login setelah berhasil menghilangkan data session
        }
    }

    public function logout() //method untuk logout
    {
        $this->session->unset_userdata('username'); //menghilangkan data yang tersimpan di session
        $this->session->unset_userdata('role'); //menghilangkan data yang tersimpan di session
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        Anda Telah Logout !
        </div>'); //membuat pesan setelah data berhasil dihilangkan dari session yang akan ditampilkan di halaman login
        redirect('auth'); //meredirect ke halam login setelah berhasil menghilangkan data session
    }

    public function blocked()
    {
        if (!$this->session->userdata('email')) { //jika di sesion ada email akan di redirect ke kontroler user / index agar tidak bisa mengakses auth
            redirect('auth/');
        }
        $data['title'] = 'Page Not Found ';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['menu'] = $this->db->get('user_menu')->result_array(); //mengambil data dari db tabel user_menu

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/navbar', $data);
        $this->load->view('auth/blocked');
        $this->load->view('templates/footer');
    }
}
