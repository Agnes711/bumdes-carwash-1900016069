<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Admin extends CI_Controller
{
  public function __construct()
          {
                  parent::__construct();
                  $this->load->helper(array('form', 'url'));
          }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session

        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/index', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    
    
    
##########################################################
    public function data_produk()
    {
        $data['title'] = 'DATA PRODUK';
        $data['kategori'] = $this->db->get('kategori')->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['produk'] = $this->db->get('produk')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/data-produk', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    public function add_data_produk()
    {
        $data['title'] = 'DATA PRODUK';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'nama' => $this->input->post('nama'),
                'kategori' => $this->input->post('kategori'),
                'harga' => $this->input->post('harga'),
            ];
        $this->db->insert('produk', $get);
        $data['kategori'] = $this->db->get('kategori')->result_array();
        $data['produk'] = $this->db->get('produk')->result_array();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Kendaraan telah ditambahkan
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/data-produk', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function delete_produk()
    {
        $data['title'] = 'DATA PRODUK';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $this->db->delete('produk',['id' => $this->input->post('del')]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
           Data Berhasil Di Delete
            </div>');
        $data['produk'] = $this->db->get('produk')->result_array();
        $data['kategori'] = $this->db->get('kategori')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/data-produk', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function edit_produk()
    {
        $data['title'] = 'DATA PRODUK';
        $data['produk'] = $this->db->get('produk')->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'nama' => $this->input->post('nama'),
                'kategori' => $this->input->post('kategori'),
                'harga' => $this->input->post('harga'),
            ];
        $this->db->where('id', $this->input->post('del'));
        $data['kategori'] = $this->db->get('kategori')->result_array();
        $this->db->update('produk', $get);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diubah
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/data-produk', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
##########################################################

##########################################################
    public function data_user()
    {
        $data['title'] = 'DATA USER';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('user')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/data-user', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    public function add_data_user()
    {
        $data['title'] = 'DATA USER';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'username' => $this->input->post('username'),
                'role' => $this->input->post('role'),
                'nama_karyawan' => $this->input->post('nama'),
                'alamat_karyawan' => $this->input->post('alamat')
            ];
        $this->db->insert('user', $get);
        $data['users'] = $this->db->get('user')->result_array();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            user telah ditambahkan
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/data-user', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function delete_user()
    {
        $data['title'] = 'DATA USER';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $this->db->delete('user',['user_id' => $this->input->post('del')]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
           Data Berhasil Di Delete
            </div>');
        $data['users'] = $this->db->get('user')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/data-user', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function edit_user()
    {
        $data['title'] = 'DATA USER';
        $data['users'] = $this->db->get('user')->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'username' => $this->input->post('username'),
                'role' => $this->input->post('role'),
                'nama_karyawan' => $this->input->post('nama'),
                'alamat_karyawan' => $this->input->post('alamat')
            ];
         $this->db->where('user_id', $this->input->post('del'));
        $this->db->update('user', $get);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diubah
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/data-user', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
##########################################################

##########################################################
    public function data_pelanggan()
    {
        $data['title'] = 'DATA PELANGGAN';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('pelanggan')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/data-pelanggan', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    public function add_data_pelanggan()
    {
        $data['title'] = 'DATA PELANGGAN';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'nama_pelanggan' => $this->input->post('nama'),
                'alamat_pelanggan' => $this->input->post('alamat'),
                'tanggal_lahir' => $this->input->post('tanggal'),
                'gender' => $this->input->post('gender'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
                'no_member' => $this->input->post('no_member'),
            ];
        $this->db->insert('pelanggan', $get);
        $data['users'] = $this->db->get('pelanggan')->result_array();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Pelanggan telah ditambahkan
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/data-pelanggan', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function delete_pelanggan()
    {
        $data['title'] = 'DATA PELANGGAN';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $this->db->delete('pelanggan',['pelanggan_id' => $this->input->post('del')]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
           Data Berhasil Di Delete
            </div>');
        $data['users'] = $this->db->get('pelanggan')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/data-pelanggan', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function edit_pelanggan()
    {
        $data['title'] = 'DATA PELANGGAN';
        $data['users'] = $this->db->get('pelanggan')->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'nama_pelanggan' => $this->input->post('nama'),
                'alamat_pelanggan' => $this->input->post('alamat'),
                'tanggal_lahir' => $this->input->post('tanggal'),
                'gender' => $this->input->post('gender'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
                'no_member' => $this->input->post('no_member'),
            ];
         $this->db->where('pelanggan_id', $this->input->post('del'));
        $this->db->update('pelanggan', $get);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diubah
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/data-pelanggan', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
##########################################################

######################################################
    public function jenis()
    {
        $data['title'] = 'JENIS CUCIAN';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('jenis')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/jenis', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    public function add_jenis()
    {
        $data['title'] = 'JENIS CUCIAN';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'jenis_nama' => $this->input->post('nama'),

            ];
        $this->db->insert('jenis', $get);
        $data['users'] = $this->db->get('jenis')->result_array();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            jenis telah ditambahkan
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/jenis', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function delete_jenis()
    {
        $data['title'] = 'JENIS CUCIAN';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $this->db->delete('jenis',['jenis_id' => $this->input->post('del')]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
           Data Berhasil Di Delete
            </div>');
        $data['users'] = $this->db->get('jenis')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/jenis', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function edit_jenis()
    {
        $data['title'] = 'JENIS CUCIAN';
        $data['users'] = $this->db->get('jenis')->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'jenis_nama' => $this->input->post('nama'),

            ];
         $this->db->where('jenis_id', $this->input->post('del'));
        $this->db->update('jenis', $get);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diubah
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/jenis', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
##########################################################

######################################################
    public function kendaraan()
    {
        $data['title'] = 'KENDARAAN';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('kendaraan')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/kendaraan', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    public function add_kendaraan()
    {
        $data['title'] = 'KENDARAAN';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'kendaraan_nama' => $this->input->post('nama'),

            ];
        $this->db->insert('kendaraan', $get);
        $data['users'] = $this->db->get('kendaraan')->result_array();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            kendaraan telah ditambahkan
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/kendaraan', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function delete_kendaraan()
    {
        $data['title'] = 'KENDARAAN';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $this->db->delete('kendaraan',['kendaraan_id' => $this->input->post('del')]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
           Data Berhasil Di Delete
            </div>');
        $data['users'] = $this->db->get('kendaraan')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/kendaraan', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function edit_kendaraan()
    {
        $data['title'] = 'KENDARAAN';
        $data['users'] = $this->db->get('kendaraan')->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'kendaraan_nama' => $this->input->post('nama'),

            ];
         $this->db->where('kendaraan_id', $this->input->post('del'));
        $this->db->update('kendaraan', $get);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diubah
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/kendaraan', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
##########################################################

######################################################
    public function metode()
    {
        $data['title'] = 'METODE';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('metode')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/metode', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    public function add_metode()
    {
        $data['title'] = 'METODE';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'metode_nama' => $this->input->post('nama'),

            ];
        $this->db->insert('metode', $get);
        $data['users'] = $this->db->get('metode')->result_array();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            metode telah ditambahkan
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/metode', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function delete_metode()
    {
        $data['title'] = 'METODE';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $this->db->delete('metode',['metode_id' => $this->input->post('del')]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
           Data Berhasil Di Delete
            </div>');
        $data['users'] = $this->db->get('metode')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/metode', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function edit_metode()
    {
        $data['title'] = 'METODE';
        $data['users'] = $this->db->get('metode')->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'metode_nama' => $this->input->post('nama'),

            ];
         $this->db->where('metode_id', $this->input->post('del'));
        $this->db->update('metode', $get);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diubah
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/metode', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
##########################################################

######################################################
    public function transaksi()
    {
        $data['title'] = 'TRANSAKSI';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('book')->result_array();
        $data['jenis_kendaraan'] = $this->db->get('kendaraan')->result_array();
        $data['jenis_cucian'] = $this->db->get('jenis')->result_array();
        $data['jenis_pembayaran'] = $this->db->get('metode')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/transaksi', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    public function add_transaksi()
    {
        $data['title'] = 'TRANSAKSI';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['pro'] = $this->db->get_where('produk', ['id' => $this->input->post('produk')])->result_array();
        $get = [ //menyiapkan data untuk di insert ke database !
                'book_nama' => $this->input->post('book_nama'),
                'harga' => $this->input->post('harga'),
                'tanggal' => date("Y-m-d"),
                'jenis_kendaraan' => $this->input->post('jenis_kendaraan'),
                'jenis_cucian' => $this->input->post('jenis_cucian'),
                'jenis_pembayaran' => $this->input->post('jenis_pembayaran'),

            ];
        print_r($get);
        $this->db->insert('book', $get);
        return redirect('admin/transaksi');
        
    }
    
    public function delete_transaksi()
    {
        $data['title'] = 'TRANSAKSI';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $this->db->delete('book',['book_id' => $this->input->post('del')]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
           Data Berhasil Di Delete
            </div>');
        return redirect('admin/transaksi');
    }
    
    public function edit_transaksi()
    {
        $data['title'] = 'TRANSAKSI';
        $data['users'] = $this->db->get('transaksi')->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['pro'] = $this->db->get_where('produk', ['id' => $this->input->post('produk')])->result_array();
        $get = [ //menyiapkan data untuk di insert ke database !
                'book_nama' => $this->input->post('book_nama'),
                'harga' => $this->input->post('harga'),
                'tanggal' => date("Y-m-d"),
                'jenis_kendaraan' => $this->input->post('jenis_kendaraan'),
                'jenis_cucian' => $this->input->post('jenis_cucian'),
                'jenis_pembayaran' => $this->input->post('jenis_pembayaran'),

            ];
         $this->db->where('book_id', $this->input->post('del'));
        $this->db->update('book', $get);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diubah
            </div>');
        return redirect('admin/transaksi');
    }
    
##########################################################

######################################################
    public function tracking()
    {
        $data['title'] = 'Lacak Barang';
        $data['track'] = array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('transaksi')->result_array();
        $data['produk'] = $this->db->get('produk')->result_array();
        $data['pelanggan'] = $this->db->get('pelanggan')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/track', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function f_tracking()
    {
        $data['title'] = 'Lacak Barang';
        $data['track'] = $this->db->get_where('transaksi', ['resi' => $this->input->post('resi')])->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('transaksi')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/track', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    
##########################################################

######################################################
    public function laporan()
    {
        $data['title'] = 'LAPORAN KEUANGAN';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('transaksi')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebar'); //menampilkan halaman sidebar.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('admin/nota', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    
##########################################################





}
