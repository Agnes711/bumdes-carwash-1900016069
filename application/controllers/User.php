<?php
defined('BASEPATH') or exit('No direct script access allowed');
class User extends CI_Controller
{
  public function __construct()
          {
                  parent::__construct();
                  $this->load->helper(array('form', 'url'));
          }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session

        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/index', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    
    
    
##########################################################
    public function data_produk()
    {
        $data['title'] = 'DATA PRODUK';
        $data['kategori'] = $this->db->get('kategori')->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['produk'] = $this->db->get('produk')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/data-produk', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    public function add_data_produk()
    {
        $data['title'] = 'DATA PRODUK';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'nama' => $this->input->post('nama'),
                'kategori' => $this->input->post('kategori'),
                'harga' => $this->input->post('harga'),
            ];
        $this->db->insert('produk', $get);
        $data['kategori'] = $this->db->get('kategori')->result_array();
        $data['produk'] = $this->db->get('produk')->result_array();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Kendaraan telah ditambahkan
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/data-produk', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function delete_produk()
    {
        $data['title'] = 'DATA PRODUK';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $this->db->delete('produk',['id' => $this->input->post('del')]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
           Data Berhasil Di Delete
            </div>');
        $data['produk'] = $this->db->get('produk')->result_array();
        $data['kategori'] = $this->db->get('kategori')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/data-produk', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function edit_produk()
    {
        $data['title'] = 'DATA PRODUK';
        $data['produk'] = $this->db->get('produk')->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'nama' => $this->input->post('nama'),
                'kategori' => $this->input->post('kategori'),
                'harga' => $this->input->post('harga'),
            ];
        $this->db->where('id', $this->input->post('del'));
        $data['kategori'] = $this->db->get('kategori')->result_array();
        $this->db->update('produk', $get);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diubah
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/data-produk', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
##########################################################

##########################################################
    public function data_user()
    {
        $data['title'] = 'DATA USER';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('user')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/data-user', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    public function add_data_user()
    {
        $data['title'] = 'DATA USER';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'username' => $this->input->post('username'),
                'role' => $this->input->post('role'),
                'nama_karyawan' => $this->input->post('nama'),
                'alamat_karyawan' => $this->input->post('alamat')
            ];
        $this->db->insert('user', $get);
        $data['users'] = $this->db->get('user')->result_array();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            user telah ditambahkan
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/data-user', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function delete_user()
    {
        $data['title'] = 'DATA USER';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $this->db->delete('user',['user_id' => $this->input->post('del')]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
           Data Berhasil Di Delete
            </div>');
        $data['users'] = $this->db->get('user')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/data-user', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function edit_user()
    {
        $data['title'] = 'DATA USER';
        $data['users'] = $this->db->get('user')->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'username' => $this->input->post('username'),
                'role' => $this->input->post('role'),
                'nama_karyawan' => $this->input->post('nama'),
                'alamat_karyawan' => $this->input->post('alamat')
            ];
         $this->db->where('user_id', $this->input->post('del'));
        $this->db->update('user', $get);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diubah
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/data-user', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
##########################################################

##########################################################
    public function data_pelanggan()
    {
        $data['title'] = 'DATA PELANGGAN';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('pelanggan')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/data-pelanggan', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    public function add_data_pelanggan()
    {
        $data['title'] = 'DATA PELANGGAN';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'nama_pelanggan' => $this->input->post('nama'),
                'alamat_pelanggan' => $this->input->post('alamat'),
                'tanggal_lahir' => $this->input->post('tanggal'),
                'gender' => $this->input->post('gender'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
                'no_member' => $this->input->post('no_member'),
            ];
        $this->db->insert('pelanggan', $get);
        $data['users'] = $this->db->get('pelanggan')->result_array();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Pelanggan telah ditambahkan
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/data-pelanggan', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function delete_pelanggan()
    {
        $data['title'] = 'DATA PELANGGAN';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $this->db->delete('pelanggan',['pelanggan_id' => $this->input->post('del')]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
           Data Berhasil Di Delete
            </div>');
        $data['users'] = $this->db->get('pelanggan')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/data-pelanggan', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function edit_pelanggan()
    {
        $data['title'] = 'DATA PELANGGAN';
        $data['users'] = $this->db->get('pelanggan')->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'nama_pelanggan' => $this->input->post('nama'),
                'alamat_pelanggan' => $this->input->post('alamat'),
                'tanggal_lahir' => $this->input->post('tanggal'),
                'gender' => $this->input->post('gender'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
                'no_member' => $this->input->post('no_member'),
            ];
         $this->db->where('pelanggan_id', $this->input->post('del'));
        $this->db->update('pelanggan', $get);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diubah
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/data-pelanggan', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
##########################################################

######################################################
    public function kategori()
    {
        $data['title'] = 'KATEGORI';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('kategori')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/kategori', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    public function add_kategori()
    {
        $data['title'] = 'KATEGORI';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'nama' => $this->input->post('nama'),

            ];
        $this->db->insert('kategori', $get);
        $data['users'] = $this->db->get('kategori')->result_array();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            kategori telah ditambahkan
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/kategori', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function delete_kategori()
    {
        $data['title'] = 'KATEGORI';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $this->db->delete('kategori',['id' => $this->input->post('del')]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
           Data Berhasil Di Delete
            </div>');
        $data['users'] = $this->db->get('kategori')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/kategori', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function edit_kategori()
    {
        $data['title'] = 'KATEGORI';
        $data['users'] = $this->db->get('kategori')->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $get = [ //menyiapkan data untuk di insert ke database !
                'nama' => $this->input->post('nama'),

            ];
         $this->db->where('id', $this->input->post('del'));
        $this->db->update('kategori', $get);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diubah
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/kategori', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
##########################################################

######################################################
    public function transaksi()
    {
        $data['title'] = 'TRANSAKSI';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('transaksi')->result_array();
        $data['produk'] = $this->db->get('produk')->result_array();
        $data['pelanggan'] = $this->db->get('pelanggan')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/transaksi', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    public function add_transaksi()
    {
        $data['title'] = 'TRANSAKSI';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['pro'] = $this->db->get_where('produk', ['id' => $this->input->post('produk')])->result_array();
        $get = [ //menyiapkan data untuk di insert ke database !
                'nama_barang' => $data['pro'][0]['nama'],
                'harga' => $data['pro'][0]['harga'],
                'nama_penjual' => $this->input->post('nama_penjual'),
                'nama_pembeli' => $this->input->post('pelanggan'),
                'jumlah' => $this->input->post('jumlah'),
                'resi' => "CD".rand(100000, 999999),
                'total' => $this->input->post('jumlah')*$data['pro'][0]['harga'],
                'tanggal_pembelian' => date("Y-m-d")

            ];
        $this->db->insert('transaksi', $get);
        $data['users'] = $this->db->get('transaksi')->result_array();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            transaksi telah ditambahkan
            </div>');
        $data['produk'] = $this->db->get('produk')->result_array();
        $data['pelanggan'] = $this->db->get('pelanggan')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/transaksi', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function delete_transaksi()
    {
        $data['title'] = 'TRANSAKSI';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $this->db->delete('transaksi',['id' => $this->input->post('del')]);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
           Data Berhasil Di Delete
            </div>');
        $data['users'] = $this->db->get('transaksi')->result_array();
        $data['produk'] = $this->db->get('produk')->result_array();
        $data['pelanggan'] = $this->db->get('pelanggan')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/transaksi', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function edit_transaksi()
    {
        $data['title'] = 'TRANSAKSI';
        $data['users'] = $this->db->get('transaksi')->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['pro'] = $this->db->get_where('produk', ['id' => $this->input->post('produk')])->result_array();
        $get = [ //menyiapkan data untuk di insert ke database !
                'nama_barang' => $data['pro'][0]['nama'],
                'harga' => $data['pro'][0]['harga'],
                'nama_penjual' => $this->input->post('nama_penjual'),
                'nama_pembeli' => $this->input->post('pelanggan'),
                'jumlah' => $this->input->post('jumlah'),
                'total' => $this->input->post('jumlah')*$data['pro'][0]['harga'],
                'tanggal_pembelian' => date("Y-m-d")

            ];
         $this->db->where('id', $this->input->post('del'));
        $this->db->update('transaksi', $get);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diubah
            </div>');
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $data['produk'] = $this->db->get('produk')->result_array();
        $data['pelanggan'] = $this->db->get('pelanggan')->result_array();
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/transaksi', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
##########################################################

######################################################
    public function tracking()
    {
        $data['title'] = 'Lacak Barang';
        $data['track'] = array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('transaksi')->result_array();
        $data['produk'] = $this->db->get('produk')->result_array();
        $data['pelanggan'] = $this->db->get('pelanggan')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/track', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    public function f_tracking()
    {
        $data['title'] = 'Lacak Barang';
        $data['track'] = $this->db->get_where('transaksi', ['resi' => $this->input->post('resi')])->result_array();
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('email')])->row_array(); // $this->session->userdata = mengambil data setelah disimpan di session
        $data['users'] = $this->db->get('transaksi')->result_array();
        $this->load->view('templates/header', $data); //menampilkan halaman header.php : struktur folder -> file
        $this->load->view('templates/sidebars'); //menampilkan halaman sidebars.php : struktur folder -> file
        $this->load->view('templates/navbar', $data); //menampilkan halaman navbar.php : struktur folder -> file
        $this->load->view('user/track', $data); //menampilkan halaman index.php : struktur folder -> file
        $this->load->view('templates/footer'); //menampilkan halaman footer.php : struktur folder -> file
    }
    
    
##########################################################







}
